<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/css/app.css"  type="text/css" rel="stylesheet">
</head>
<body>
<div class="container">

    {{--<tasks></tasks>--}}

    <alert type="error">
        <strong>Success!</strong> Indicates a successful or positive action.
    </alert>

</div>

{{--<template id="tasks-template">--}}
    {{--<div>--}}
        {{--<h1>My Tasks</h1>--}}
        {{--<ul class="list-group">--}}
            {{--<li class="list-group-item" v-for="task in list">--}}
                {{--@{{ task.body }}--}}
                {{--<strong @click="deleteTask(task)">x</strong>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</template>--}}



<script src="/js/main.js"></script>

</body>
</html>
