//browserfy entrypoint

var Vue = require('vue/dist/vue.js');
var VueResource = require('vue-resource');
Vue.use(VueResource);

import Alert from './components/Alert.vue';

Vue.http.headers.common['X-CSRF-TOKEN'] = (document.querySelector('input[name="_token"]')) ? document.querySelector('input[name="_token"]').value : '';

var ajax = Vue.directive('ajax', {
    bind: function (el, binding, vnode) {
        console.log(el);
        el.addEventListener('submit', ajax.onSubmit.bind(ajax));
    },

    onSubmit: function (e) {
        e.preventDefault();

        var el = e.target;

        vm.$http[ajax.getRequestType(el)](el.action)
            .then(ajax.onComplete.bind(ajax), ajax.onError.bind(ajax));
    },

    getRequestType: function (el) {
        var method = el.querySelector('input[name="_method"]');

        return (method ? method.value : ajax.el.method).toLowerCase();
    },

    onComplete: function () {
        alert('onComplete');
    },

    onError: function (response) {
        alert(response.status + ' ' + response.statusText);
    }
});

var vm = new Vue({
    el: '.container',

    components: {Alert},

    ready() {
        console.log('Ready to go!');
    }
});

// new Vue({
//     el: '#directive-lesson',
//
//     ready() {
//         console.log('Directive ready!');
//     }
// });

