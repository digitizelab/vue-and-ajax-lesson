<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/css/app.css"  type="text/css" rel="stylesheet">
</head>
<body>
<div class="container">

    <form method="POST" action="/post/2" v-ajax>
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit">Delete Post</button>
    </form>


</div>

<script src="/js/main.js"></script>

</body>
</html>
